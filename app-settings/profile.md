<!-- TITLE: Profile -->
<!-- SUBTITLE: A quick summary of Profile -->

The profile section requires basic business information such as Your business Name, address, Country (essential for address verification) and Currency (essential for invoicing and payments)

![Profile](/uploads/settings/profile.png "Profile")

Please do select the option to be contacted about Squeegee as we don't send out information to our users very often, but when we do, it's always relevant, generally talking about new features and sometimes includes offers!