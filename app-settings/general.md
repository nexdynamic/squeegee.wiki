<!-- TITLE: General -->
<!-- SUBTITLE: A quick summary of General -->

The options included in General settings covers how your app looks to you, and if you're on an Enterprise plan, somee of the main permissions for workers (Prices and Owing View) 

![General Settings](/uploads/settings/general-settings.png "General Settings")

Your business logo will appear on invoices once uploaded so it's important to include this. 

The work sign off option is a global setting that can be set or over-ridden individually from the customer edit screen. 

This option makes the sign off dialog show on completing jobs:

![Job Sign Off](/uploads/settings/job-sign-off.png "Job Sign Off")