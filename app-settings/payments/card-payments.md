<!-- TITLE: Card Payments -->
<!-- SUBTITLE: A quick summary of Card Payments -->

Our Stripe integration allows customers to pay invoices online straight from the invoice itself via a 'pay with card' button. 

![Pay With Card](/uploads/settings/pay-with-card.png "Pay With Card")

Customers can enter their card details and save them to speed up the process of payment for next time. 

![Card Payment Form](/uploads/settings/card-payment-form.png "Card Payment Form")

Stripe processing fees are 1.4% + 20p for European cards and 2.4% + 20p for non-European cards. For more details take a look at their pricing page: https://stripe.com/gb/pricing and set up your account ready for linking with Squeegee