<!-- TITLE: Direct Debit Payments -->
<!-- SUBTITLE: A quick summary of Direct Debit Payments -->

GoCardless is the fastest and easiest way to collect payments from customers within Squeegee. It can be set to charge customers automatically on swiping a job as done saving lots of time and effort chasing payments. 

GoCardless processing fees are 1% + 20p for charges made in the UK and 2% + 20p on international charges. For more details take a look at their pricing page: https://gocardless.com/pricing/ 

The first thing you need to do is either link your existing GoCardless account opr create a new one from within the app:

![Create Or Sign Into Gc](/uploads/settings/create-or-sign-into-gc.png "Create Or Sign Into Gc")

Once your account is active, you can invite customers via email or sms to set up as a GoCardless Customer and mandate to you. 

A useful Guide on getting customers to sign up to GoCardless outlining the benefits can be found here: https://gocardless.com/guides/moving-customers-to-direct-debit/when-to-ask-customers-to-switch/

The link on the customer within Squeegee is unique so that once you have sent it out and the customer signs up, you don't have to do anything at all. The process takes a few days for customers to become active (around 3-5 working days) and as soon as they are active, you can start making charges. 

To invite a customer, go to the customer edit menu (three dots to the top right of the customer) and press direct debit

![Invite To Gc 1](/uploads/settings/invite-to-gc-1.png "Invite To Gc 1")

Then choose how to send the lin, either email, sms or copy the link to send a different way and choose whether to have the customer set to make a direct debit charge automatically fromt this screen that will come into effect once they are active (you can always go into the customer and edit this afterwards) 

![Invite To Gc 2](/uploads/settings/invite-to-gc-2.png "Invite To Gc 2")

