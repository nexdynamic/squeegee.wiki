<!-- TITLE: Appointment Reminders -->
<!-- SUBTITLE: A quick summary of Appointment Reminders -->

# Appointment Reminders
Appointment reminders in Squeegee allow you to bulk send templated appointment reminders to your customers for any day in the future, including today.

There are three parts to appointment reminders:

- [Setting up the appointment reminder template](#appointment-reminder-templates)
- [Setting up your customers default notification method](#setting-up-customers-and-sending-reminders)
- [Sending the reminders on a specific day](#sending-reminders-and-sending-reminders)

## Appointment Reminder Templates
There are two templates for appointment reminders:
- Email Appointment Reminder Default
- SMS Appointment Reminder Default

These are access from the settings screen in the "Notifications" section:
![Appointment Reminder Templates](/uploads/work-planner/appointment-reminder-templates.png "Appointment Reminder Templates")

If you click or touch these you can edit the default message and include a number of built in tokens which will get replaced when the message is created. 

These following tokens are included but these may be updated from time to time:
| Token | Description |
|---|---|
| [customerName] | The full name as entered for the customer e.g. "Mr John Smith" |
| [firstname] | The first name if the customer if they have one or more names e.g. "John" |
| [lastname] | The last name of the customer if they have two or more e.g. "Smith" |
| [title] | The title of the customer if there is one e.g. "Mr" |
| [dateFromNow] | The date expressed as text describing how far from now it is e.g. "today", "tomorrow" or "in 5 days" |
| [formattedDate] | The date formatted to easily readable text e.g. "Thursday, 3rd August" |
| [serviceList] | A simple list of the services e.g. "Outside Windows, Inside Windows and Greenhouse" |
| [addressDescription] | The full text of the address e.g "The Old Police House, Hyde Park, London, W2 2UH"  |
| [formattedPrice] | The formatted price of the appointment e.g. "£15.00" or "$47.00" |
| [roundName] | The round or job group name that this appointment is part of if any e.g."London SW1" or "Westcliffe 4 Weekly" |
| [assignedTo] | The email address of the worker that this job is assigned to, this should not be used for teams as it will show the team ID |
| [duration] | The estimated duration of the appointment as specified on the job |
| [nextDate] | The next date following this appointment formatted to easily readable text e.g. "Thursday, 3rd September" |
| [viewOnline] | A link to view this notification online |

![Email Appointment Reminder Template](/uploads/work-planner/email-appointment-reminder-template.png "Email Appointment Reminder Template")

Once you have set your appointment reminder template, you are ready to set up some customers to receive them.

## Setting Up Customers And Sending Reminders
To send reminders, a customer needs a default notification method set. Once this is set they will appear in the appointment reminder list generated when you send appointment reminders from the work planner.

[video](/uploads/work-planner/appointment-reminders.webm){.video}


