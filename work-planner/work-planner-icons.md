<!-- TITLE: Work Planner Icons -->
<!-- SUBTITLE: A quick summary of the work planner icons and what each of them mean. -->

General Icons
| Icon | Name | Description |
|---|---|---|
| ![Notes Icon](/uploads/work-planner-icons/notes.png "Notes") | **Job Notes** | There are notes attached to this job |
| ![Customer Owing Icon](/uploads/work-planner-icons/customer-owing.png "Customer Owing") | **Customer Owing** | This customer currently has an outstanding balance owing |
| ![Customer in Credit Icon](/uploads/work-planner-icons/customer-in-credit.png "Customer in Credit") | **Customer in Credit** | This customer is currently in credit |
| ![First Appointment Icons](/uploads/work-planner-icons/first-appointment.png "First Appointment") | **First Appointment** | This is either the very first appointment or the first since the schedule was reset |
| ![Automatic Invoicing Icon](/uploads/work-planner-icons/automatic-invoicing.png "Auto Invoicing") | **Auto Invoicing** | Automatic invoicing is switched on for this customer regardless of your global setting |
| ![Debit on Invoice Icon](/uploads/work-planner-icons/debit-on-invoice.png "Debit on Invoice") | **Debit on Invoice** | This customer is automatically debited using GoCardless when an invoice is generated |
| ![Important Tag Icon](/uploads/work-planner-icons/important-tag.png "Important Tag") | **Important Tag** | This prefixes an important tag in the customer notes that you prefixed with #! |

Payment Types
| Icon | Name | Description |
|---|---|---|
| ![Cheque Icon](https://app.squeeg.ee/icons/solid/payment.cheque.svg "Cheque") | **Cheque** | This customer last paid by cheque |
| ![Bank Transfer Icon](https://app.squeeg.ee/icons/solid/payment.bank-transfer.svg "Bank Transfer") | **Bank Transfer** | This customer last paid by bank transfer |
| ![Cash Icon](https://app.squeeg.ee/icons/solid/payment.cash.svg "Cash") | **Cash** | This customer last paid by cash |
| ![Manual Card Icon](https://app.squeeg.ee/icons/solid/payment.card.svg "Manual Card") | **Manual Card** | This customer last paid by manual card payment |
| ![GoCardless Icon](https://app.squeeg.ee/icons/solid/auto.go-cardless.svg "GoCardless") | **GoCardless** | This customer last paid by GoCardless |
| ![Stripe Icon](https://app.squeeg.ee/icons/solid/payment.stripe.svg "Stripe") | **Stripe** | This customer last paid by Stripe |
