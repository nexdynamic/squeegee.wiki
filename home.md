<!-- TITLE: The Squeegee Wiki Home -->
<!-- SUBTITLE: This is the homepage of the Squeegee Platform and App documentation wiki -->

# Welcome to the Squeegee Wiki
This is the home for the official Squeegee platform and app documentation.

It will grow and update over time so please make requests for things you do not find here.

Also, please let us know if you spot any errors or inconsistencies, we know they're there somewhere :smirk:

[Click here to view the documentation listing](/all "View the documentation listing")

Alternatively use the search above to get straight to the article you are after.
